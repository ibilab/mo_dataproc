package net.infobank.itv.mo_dataproc.model;

public class Statistics {

    private int ch_key;
    private int pgm_key;
    private String msg_com;
    private int stat_new;
    private int stat_tot;
    private int stat_mo;
    private int stat_mt;
    private String stat_date;
    
    
    @Override
    public String toString() {
        return "Statistics [ch_key=" + ch_key + ", pgm_key=" + pgm_key + ", msg_com=" + msg_com + ", stat_new=" + stat_new + ", stat_tot=" + stat_tot + ", stat_mo=" + stat_mo + ", stat_mt=" + stat_mt
                + ", stat_date=" + stat_date + "]";
    }
    
    public int getCh_key() {
        return ch_key;
    }
    public void setCh_key(int ch_key) {
        this.ch_key = ch_key;
    }
    public int getPgm_key() {
        return pgm_key;
    }
    public void setPgm_key(int pgm_key) {
        this.pgm_key = pgm_key;
    }
    public String getMsg_com() {
        return msg_com;
    }
    public void setMsg_com(String msg_com) {
        this.msg_com = msg_com;
    }
    public int getStat_new() {
        return stat_new;
    }
    public void setStat_new(int stat_new) {
        this.stat_new = stat_new;
    }
    public int getStat_tot() {
        return stat_tot;
    }
    public void setStat_tot(int stat_tot) {
        this.stat_tot = stat_tot;
    }
    public int getStat_mo() {
        return stat_mo;
    }
    public void setStat_mo(int stat_mo) {
        this.stat_mo = stat_mo;
    }
    public int getStat_mt() {
        return stat_mt;
    }
    public void setStat_mt(int stat_mt) {
        this.stat_mt = stat_mt;
    }
    public String getStat_date() {
        return stat_date;
    }
    public void setStat_date(String stat_date) {
        this.stat_date = stat_date;
    }
    
}
