package net.infobank.itv.mo_dataproc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import net.infobank.itv.mo_common.model.Schedule;
import net.infobank.itv.mo_common.util.FileConfig;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_dataproc.util.ScheduleUtils;
import net.infobank.itv.mo_dataproc.util.StatisticsUtils;

public class Mo_DataProc {
    private static Logger log = LoggerFactory.getLogger(Mo_DataProc.class);

    public static void main(String[] args) {
        Mo_DataProc data_proc = new Mo_DataProc();
        data_proc.start(args);
    }
    
    public void start(String[] args) {
        FileConfig _config;

        // 접속정보 를 파일에서 가져온다.
        // 접속정보 를 파일에서 가져온다.
        if (args.length < 1) {
            System.err.println("error: not enough argument");
            System.err.println("<arg1:config file>");
            System.err.println("config ymd");
            return;
        }
        
        _config = new FileConfig(args[0]);
        
        String group_name = new Object() {}.getClass().getEnclosingClass().getSimpleName();
        MDC.put("log_file", _config.getLog_dir() + "/" + group_name+ "/" + group_name);
        
        Properties props = new Properties();
        props.put("driver", _config.getDriver());
        props.put("url", _config.getUrl());
        props.put("user", _config.getUser());
        props.put("password", _config.getPw());
        MyBatisConnectionFactory.setSqlSessionFactory(props);
        
        String version = _config.getVersion(); // 버전 
        int nVersion = Integer.parseInt(version); 
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        Date date = new Date();
        String stat_datetime = sdf.format(date);
        
        if (args.length > 1) {
        	stat_datetime = args[1];
        	if(stat_datetime.length() < 12) stat_datetime += "0000";
        }
        
        
        if(nVersion == 2)  { 
        	ArrayList<Schedule> schedule_list = (ArrayList<Schedule>) ScheduleUtils.getMOSchedule2();
        	
	        try {
	            for(int i=0 ; i<schedule_list.size() ; i++) {
	                Schedule s = schedule_list.get(i);
	                int ch_key = s.getCh_key();
	                int pgm_key = s.getPgm_key();
	                
	                
	                StatisticsUtils.procStatistics2(ch_key, pgm_key, stat_datetime);
	        
	            }
	            log.info("Data Process {} End", stat_datetime);
	        } catch (Exception e) {
	            log.error("Exception", e);
	        } 
        }
        else  { 
        	ArrayList<Schedule> schedule_list = (ArrayList<Schedule>) ScheduleUtils.getMOSchedule();
        
	        try {
	            for(int i=0 ; i<schedule_list.size() ; i++) {
	                Schedule s = schedule_list.get(i);
	                int ch_key = s.getCh_key();
	                int pgm_key = s.getPgm_key();
	                String sns_list = s.getSns_list();
	        
	                String[] sns_lists = sns_list.split("\\|");
	
	                for(int j=0 ; j<sns_lists.length ; j++) {
	                    String msg_com = sns_lists[j];
	                    StatisticsUtils.procStatistics(ch_key, pgm_key, msg_com, stat_datetime);
	                }
	            }
	            log.info("Data Process {} End", stat_datetime);
	        } catch (Exception e) {
	            log.error("Exception", e);
	        } 
        }
        
    }
}
