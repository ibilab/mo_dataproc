package net.infobank.itv.mo_dataproc.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_dataproc.model.Statistics;

public class StatisticsDAO {
    private static Logger log = LoggerFactory.getLogger(StatisticsDAO.class);
    private SqlSessionFactory sqlSessionFactory = null;

    public StatisticsDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public int procStatistics(int ch_key, int pgm_key, String msg_com, String stat_datetime) {
        int count = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("ch_key", ch_key);
            paramMap.put("pgm_key", pgm_key);
            paramMap.put("msg_com", msg_com);
            paramMap.put("stat_date", stat_datetime.substring(0, 8));
            paramMap.put("stat_datetime", stat_datetime);

            // select statistic_history
            int stat_new = 0;
            int stat_tot = 0;
            int stat_mo = 0;
            int stat_mt = 0;
            Statistics stat = session.selectOne("Statistics.selectHistoryOne", paramMap);
            if(stat != null) {
               stat_new = stat.getStat_new();
               stat_tot = stat.getStat_tot();
               stat_mo = stat.getStat_mo();
               stat_mt = stat.getStat_mt();
            }

            Statistics now = session.selectOne("Statistics.selectCounts", paramMap);
            
            // insert statistics
            paramMap.put("stat_new", now.getStat_new() - stat_new);
            paramMap.put("stat_tot", (now.getStat_mo() + now.getStat_mt()) - stat_tot);
            paramMap.put("stat_mo", now.getStat_mo() - stat_mo);
            paramMap.put("stat_mt", now.getStat_mt() - stat_mt);
            
            session.insert("Statistics.insertStatistics", paramMap);
            session.insert("Statistics.insertStatisticsHistory", paramMap);
            session.commit();
        } catch(Exception e)  {
            log.error("Statistics.procStatistics : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return count;
    }
    
    public int procStatistics2(int ch_key, int pgm_key, String stat_datetime) {
        int count = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("ch_key", ch_key);
            paramMap.put("pgm_key", pgm_key);
            paramMap.put("stat_date", stat_datetime.substring(0, 8));
            paramMap.put("stat_datetime", stat_datetime);

            // select statistic_history
            int stat_new = 0;
            int stat_tot = 0;
            int stat_mo = 0;
            int stat_mt = 0;
            
            /*
            Statistics stat = session.selectOne("Statistics.selectHistoryOne2", paramMap);
            if(stat != null) {
               stat_new = stat.getStat_new();
               stat_tot = stat.getStat_tot();
               stat_mo = stat.getStat_mo();
               stat_mt = stat.getStat_mt();
            }
            */

            Statistics now = session.selectOne("Statistics.selectCounts2", paramMap);
            log.info("procStatistics2 : " + now.toString());
            // insert statistics
            paramMap.put("stat_new", now.getStat_new());
            paramMap.put("stat_tot", (now.getStat_mo() + now.getStat_mt()));
            paramMap.put("stat_mo", now.getStat_mo());
            paramMap.put("stat_mt", now.getStat_mt());
            
            session.insert("Statistics.insertStatistics2", paramMap);
            session.commit();
        } catch(Exception e)  {
            log.error("Statistics.procStatistics2 : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return count;
    }

}
