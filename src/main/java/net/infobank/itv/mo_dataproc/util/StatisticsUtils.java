package net.infobank.itv.mo_dataproc.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_dataproc.dao.StatisticsDAO;

public class StatisticsUtils {
    private static Logger log = LoggerFactory.getLogger(StatisticsUtils.class);
    
    public static int procStatistics(int ch_key, int pgm_key, String msg_com, String stat_datetime) {
        int id = -1;
        StatisticsDAO StatisticsDAO = new StatisticsDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = StatisticsDAO.procStatistics(ch_key, pgm_key, msg_com, stat_datetime);
        } catch(Exception e)  {
            log.error("Statistics.procStatistics : " , e);            
        }
        return id;
    }
    
    public static int procStatistics2(int ch_key, int pgm_key, String stat_datetime) {
        int id = -1;
        StatisticsDAO StatisticsDAO = new StatisticsDAO(MyBatisConnectionFactory.getSqlSessionFactory());

        try {
        	id = StatisticsDAO.procStatistics2(ch_key, pgm_key, stat_datetime);
        } catch(Exception e)  {
            log.error("Statistics.procStatistics : " , e);            
        }
        return id;
    }

}
