package net.infobank.itv.mo_dataproc.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.model.Schedule;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_dataproc.dao.ScheduleDAO;

public class ScheduleUtils {
    private static Logger log = LoggerFactory.getLogger(ScheduleUtils.class);
    
    public static List<Schedule> getMOSchedule() {
        ScheduleDAO scheduleDAO = new ScheduleDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        List<Schedule> list = new ArrayList<Schedule>();
        try {
            list = scheduleDAO.selectMoSchedule();
        } catch (Exception e) {
            log.error("Exception getMOSchedule : ", e);
        }
        return list;
    }
    
    public static List<Schedule> getMOSchedule2() {
        ScheduleDAO scheduleDAO = new ScheduleDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        List<Schedule> list = new ArrayList<Schedule>();
        try {
            list = scheduleDAO.selectMoSchedule2();
        } catch (Exception e) {
            log.error("Exception getMOSchedule2 : ", e);
        }
        return list;
    }
}
